.PHONY: build test clean

build:
	docker-compose -f docker-compose.test.yaml build

test:
	trap 'make clean' EXIT; docker-compose -f docker-compose.test.yaml run utils test

clean:
	docker-compose -f docker-compose.test.yaml down --remove-orphans
