import asyncio

import websockets

from utils.aiotools import run_cascade, chain
from utils.ws import WsClient


# TODO: async setUp
# - run parallel
# - run before
# - run after


class WsProxy(WsClient):
    def __init__(self, url, host, port):
        super(WsProxy, self).__init__()

        self.url = url
        self.ping_timer = 0

        self.received_messages = []
        self.sent_messages = []

        self.host = host.rstrip('/')
        self.port = port

        self.public_url = 'ws://{}:{}/'.format(self.host, self.port)
        self.remote_ws = None
        self.server = None

    async def serve(self):
        async def _get_server():
            self.server = await websockets.serve(self._echo, self.host, self.port)

        await asyncio.gather(_get_server(),
                             self.connect())

    async def on_connect(self):
        # send all buffered (and newly buffered) messages to send
        i = 0
        while i < len(self.sent_messages):
            await self.ws.send(self.received_messages[i])
            i += 1

    async def _echo(self, ws, path):
        # send all buffered (and newly buffered) messages to retrieve
        i = 0
        while i < len(self.received_messages):
            await ws.send(self.received_messages[i])
            i += 1

        # run echo server
        self.remote_ws = ws
        async for message in ws:
            self.sent_messages.append(message)
            if self.ws:
                await self.ws.send(message)

    async def handle_message(self, msg):
        self.received_messages.append(msg)
        if self.remote_ws:
            await self.remote_ws.send(msg)

    async def close(self):
        await super(WsProxy, self).close()
        if self.server:
            self.server.close()
            self.server = None


def with_setup_and_finish(fn):
    async def wrapper(*args, **kwargs):
        return await run_cascade(args[0]._setup, chain(fn(*args, **kwargs), args[0]._finish))
    return wrapper
