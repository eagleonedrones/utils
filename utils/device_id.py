import hashlib

from rsa import PublicKey

from utils.crypting import export_key_pub


def get_device_id_from_key_pub(key_pub):
    if isinstance(key_pub, PublicKey):
        key_pub = export_key_pub(key_pub)

    for char_to_replace in ['\n', '\r', '\t', ' ', '-']:
        key_pub = key_pub.replace(char_to_replace, '')

    key_pub = key_pub.lower()
    return hashlib.md5(key_pub.encode('UTF8')).hexdigest()[:6]
