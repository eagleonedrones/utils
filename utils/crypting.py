import os
import rsa

BASE_DIR = os.environ['BASE_DIR']
RSA_KEYS_BITS = int(os.environ.get('RSA_KEYS_BITS', 2048))
RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', 'keys')


def get_rsa_keys(bits: int = RSA_KEYS_BITS, pair_name: str = None) -> (rsa.PublicKey, rsa.PrivateKey):
    """
    Generates pair of RSA keys stored by `pair_name`
    """

    if pair_name:
        # try to obtain public and private key
        try:
            with open(os.path.join(BASE_DIR, RSA_KEYS_PATH, f'{pair_name}_rsa.pub'), 'rb') as f:
                key_pub = rsa.PublicKey.load_pkcs1(f.read(), format='PEM')
            with open(os.path.join(BASE_DIR, RSA_KEYS_PATH, f'{pair_name}_rsa'), 'rb') as f:
                key = rsa.PrivateKey.load_pkcs1(f.read(), format='PEM')

        except (FileNotFoundError, ValueError):
            key_pub, key = rsa.newkeys(bits)
            with open(os.path.join(BASE_DIR, RSA_KEYS_PATH, f'{pair_name}_rsa.pub'), 'wb') as f:
                f.write(key_pub.save_pkcs1(format='PEM'))
            with open(os.path.join(BASE_DIR, RSA_KEYS_PATH, f'{pair_name}_rsa'), 'wb') as f:
                f.write(key.save_pkcs1(format='PEM'))

        return key_pub, key
    return rsa.newkeys(bits)


def encrypt(text: str, key_pub: rsa.PublicKey):
    return rsa.encrypt(text.encode('utf8'), key_pub)


def decrypt(text: bytes, key: rsa.PrivateKey):
    return rsa.decrypt(text, key).decode('utf8')


def export_key_pub(key_pub: rsa.PublicKey):
    return key_pub.save_pkcs1(format='PEM').decode('utf8')


def import_key_pub(key_pub_utf: str):
    return rsa.PublicKey.load_pkcs1(key_pub_utf.encode('utf8'), format='PEM')
