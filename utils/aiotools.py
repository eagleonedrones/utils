import asyncio

import time


async def sleep_until(cb, timeout=5):
    timeout += time.time()
    while not cb():
        await asyncio.sleep(0.05)
        if timeout < time.time():
            raise TimeoutError


def run_in_sync(coro):
    def wrapper(*args, **kwargs):
        loop = asyncio.new_event_loop()
        return loop.run_until_complete(coro(*args, **kwargs))
    return wrapper


async def chain(*awaitables):
    for awaitable in awaitables:
        await awaitable


async def run_cascade(*awaitables, delay: float=.1):
    await asyncio.gather(*[
        chain(asyncio.sleep(i * delay), awaitable)
        for i, awaitable in enumerate(awaitables)
    ])
