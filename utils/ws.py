import asyncio

import websockets
from websockets import ConnectionClosed


class WsClient(object):
    def __init__(self):
        self.url = None
        self.ping_timer = 5
        self.ws = None

    async def handle_message(self, msg):
        raise NotImplementedError('handle_message must be implemented')

    async def ping(self):
        raise NotImplementedError('ping must be implemented')

    async def on_connect(self):
        raise NotImplementedError('on_connect must be implemented')

    async def connect(self):
        await asyncio.gather(self._connect(),
                             self._pinger())

    async def _connect(self):
        print('trying to connect')
        async with websockets.connect(self.url) as ws:
            self.ws = ws
            print('connected', self.url)
            await self.on_connect()
            await self.run()

        self.ws = None

    async def close(self):
        if self.ws:
            await self.ws.close()
            self.ws = None
            print('connection closed')

    async def run(self):
        while True:
            if not self.ws:
                break

            try:
                msg = await self.ws.recv()
            except ConnectionClosed:
                await self.close()

            else:
                if msg is None:
                    await self.close()
                    break

                else:
                    await self.handle_message(msg)

    async def _pinger(self):
        while self.ping_timer:
            await asyncio.sleep(self.ping_timer)
            if self.ws:
                await self.ping()
            else:
                break
