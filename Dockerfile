FROM python:alpine3.7

RUN mkdir app
WORKDIR /app

RUN apk add --no-cache db-dev

ARG BUILD_DEPENDENCIES="build-base"

RUN apk add --no-cache ${BUILD_DEPENDENCIES} \
 && python3 -m venv /venv

# set latest pip version to avoid warning messages
RUN /venv/bin/pip install pip==19.1

COPY requirements.txt /requirements.txt
RUN /venv/bin/pip install -r /requirements.txt

RUN apk del ${BUILD_DEPENDENCIES}

# Copy files
COPY . .

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
