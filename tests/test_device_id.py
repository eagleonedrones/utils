import unittest

from utils.crypting import get_rsa_keys
from utils.device_id import get_device_id_from_key_pub


class DeviceIdTestCase(unittest.TestCase):
    @staticmethod
    def _generate_id(i):
        return get_device_id_from_key_pub(get_rsa_keys(pair_name=f'test_pair_{i}')[0])

    def test_generated_ids_are_unique(self):
        count = 5
        ids = {self._generate_id(i) for i in range(count)}
        self.assertEqual(count, len(ids))
