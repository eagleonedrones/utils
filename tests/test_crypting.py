import unittest

from utils.crypting import decrypt, encrypt, get_rsa_keys


class CryptingTestCase(unittest.TestCase):
    def setUp(self):
        self.key_pub, self.key = get_rsa_keys(pair_name='test_pair_1')
        self.text_to_test = 'text to test'

    def test_decrypted_text_should_equal_text_before_encryption(self):
        self.assertEqual(self.text_to_test, decrypt(encrypt(self.text_to_test, self.key_pub), self.key))

    def test_encrypted_text_is_not_equal_to_original_text(self):
        self.assertNotEqual(self.text_to_test, encrypt(self.text_to_test, self.key_pub))
