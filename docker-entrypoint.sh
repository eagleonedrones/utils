#!/bin/bash

set -e

# Commands available using `docker-compose run utils [COMMAND]`
case "$1" in
    python)
        /venv/bin/python
    ;;
    test)
        /venv/bin/python -m unittest
    ;;
esac
